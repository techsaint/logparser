# README #


### USAGE ###

Run Main.rb -f logfile  
  this will output the ID and Subject of all log entries
you can also do
   Main.rb -f logfile -s ID,SUBJECT,MESSAGE,DATE_TIME
or
   Main.rb --file logfile --selection ID,MESSAGE

### Tests ###
you can run logparser_test.rb to run the spec tests