require 'optparse' #added to parse commandline
require 'ostruct'  #added for struct usage


#encapsulate parsing logic for better unit testing
class LogParser
  attr_accessor :options, :logfile

  DATA_TYPES = %w[ID SUBJECT MESSAGE DATE TIME DATE_TIME]

  def initialize(argv)
    # The options specified on the command line will be collected in *options*.
    self.options = OpenStruct.new
    self.options.file = "" #se
    self.options.selection= %w[ID SUBJECT] #set default values for column select
    self.logfile = Array.new #initialize logfile array
    self.parseCMDLine(argv)

  end
  def parseCMDLine(argv)

   optionparser= OptionParser.new do |params|
      params.banner = "Usage: logparser.rb -f <log_filename> -s [columns]"
      params.on("-h", "--help", "prints help") do
        puts params
        exit
      end
      params.on("-f FILE", "--file FILE", "Full path of FILE to parse") do |file|
        self.options.file = file
      end

      params.on("-s ID,SUBJECT,MESSAGE", "--select", Array, "Which colums to display |" +DATA_TYPES.join(",") ) do |list|
        self.options.selection = list
      end
    end
   optionparser.parse!(argv) #begin the command arg parsing
   optionparser
  end

  #parse the log file given
  def parseLogfile()
    # Apply a quick check to confirm file exists
    if !self.options.file then
      puts "File was not given. Please use -f <filename>" #if file doesnt exist, give error
      exit 1 #exit with an error code
    end
      # Apply a quick check to confirm file exists
      if !File.exists?(self.options.file) then
        puts "File does not exist." #if file doesnt exist, give error
        exit 1 #exit with an error code
      end

      #Open log file, read only is default
      File.open(self.options.file) do |file_EOL|
        while (str_EOL = file_EOL.gets) #while file has a valid line

          #split line by spaces up until the fifth space
          items = "#{str_EOL}".split(' ', 5)
          self.logfile.push({
                           :date => items[0], #First position is date
                           :time => items[1], #second position is time
                           :id => items[2], #third position is the ID
                           :subject => items[3], #forth position is subject
                           :msg => items[4] #last position and all other spaces
                       })
        end
      end
  end

  #print the logfile array to the console
  def print()
    self.logfile.each do |line|
      output = []
      if options.selection.include? "ID" then
        output.push(line[:id])
      end
      if options.selection.include? "SUBJECT" then
        output.push(line[:subject])
      end
      if options.selection.include? "DATE" then
        output.push(line[:date])
      end
      if options.selection.include? "DATE_TIME" then
        output.push("#{line[:date]} #{line[:time]}")
      end
      if options.selection.include? "MESSAGE" then
        output.push(line[:msg])
      end
      puts output.join(" ")
    end
  end

  #parse the log file and print output to the console
  def parseAndPrint()
    parseLogfile() #parse log file into logfile array
    print() #print logfile array to console
  end
end