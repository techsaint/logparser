#This is a unit test built using minitest. rspec seemed a bit to
#heavy weight for such a project and in such short time

require 'minitest/autorun'
require_relative 'logparser.rb'

describe LogParser do
  before do
    @argv_mock = %w(-f test.log -s ID,SUBJECT  )
    @selection = %(ID SUBJECT)
  end

  describe "when given a file" do
    it "must properly store file path" do
     parser = LogParser.new @argv_mock
     refute_empty parser.options.file #shouldnt be empty
     assert_match /^test.log/i, parser.options.file

    end
  end

  describe "when given a set of columns ID and SUBJECT" do
    it "must properly parse and store them" do
      parser = LogParser.new @argv_mock
      refute_empty parser.options.selection
      assert_match /^ID/i, parser.options.selection[0]
      assert_match /^SUBJECT/i, parser.options.selection[1]
    end
  end
  describe "when given a valid argument list" do
    it "must generate logfile array from given log file" do
      parser = LogParser.new @argv_mock
      parser.parseLogfile()
      refute_empty parser.logfile
      assert_match /^\[10066\]/i, parser.logfile[0][:id]
    end
  end
end
