#main.rb will run the parser
# usage -f <filename> -s ID,SUBJECT,MESSAGE

require_relative 'logparser.rb'

#Execute Log File Parser
parser = LogParser.new ARGV
parser.parseAndPrint()
